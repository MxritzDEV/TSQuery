package de.m0xitz.tsquery.command;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;

public abstract class Command {

    private String name;
    private String description;

    protected Command( String name, String description ) {
        this.name = name;
        this.description = description;
    }

    public abstract void execute( ClientInfo clientInfo, String... strings );

    public String getName( ) {
        return name;
    }

    public String getDescription( ) {
        return description;
    }
}
