package de.m0xitz.tsquery.support;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.google.common.collect.Maps;
import de.m0xitz.tsquery.Main;
import de.m0xitz.tsquery.command.Command;

import java.util.HashMap;

public class SupportManager {

    private Main main;
    private HashMap< Integer, SupportChannel > supports;

    public SupportManager( Main main ) {
        this.main = main;

        init( );
    }

    private void init( ) {
        supports = Maps.newHashMap( );
        registerCommands( );

        /*
        - !support reload
        - !support accept <id>
        - !support deny <id>
        - !support open
        - !support close


        - !support stats <User>
         */

        /*  ~ Support successfully ~
        - User wird nach seinen Problem angeschrieben
        - User beantwortet
        - Channel wird erstellt und User gemovet
        - Teamler werden benachrichtigt
         */

    }

    private void registerCommands( ) {
        main.getCommandManager( ).registerCommand( new Command( "support", "" ) {
            @Override
            public void execute( ClientInfo clientInfo, String... strings ) {
                if ( !main.getTeamspeakManager( ).isInServerTeam( clientInfo.getDatabaseId( ) ) ) {
                    main.getCommandManager( ).sendMessage( clientInfo, "You don't have permission to perform this command." );
                    return;
                }
                if ( strings.length == 2 ) {
                    if ( strings[0].equalsIgnoreCase( "accept" ) ) {
                        try {
                            accept( clientInfo, Integer.parseInt( strings[1] ) );
                        } catch ( NumberFormatException ex ) {
                            main.getCommandManager( ).sendMessage( clientInfo, "Bitte gibt eine Zahl ein." );
                        }
                        return;
                    }
                    if ( strings[0].equalsIgnoreCase( "deny" ) ) {
                        try {
                            deny( clientInfo, Integer.parseInt( strings[1] ) );
                        } catch ( NumberFormatException ex ) {
                            main.getCommandManager( ).sendMessage( clientInfo, "Bitte gibt eine Zahl ein." );
                        }
                        return;
                    }
                }
                if ( strings.length == 1 ) {
                    if ( strings[0].equalsIgnoreCase( "open" ) ) {
                        open( clientInfo );
                        return;
                    }
                    if ( strings[0].equalsIgnoreCase( "close" ) ) {
                        close( clientInfo );
                        return;
                    }
                    if ( strings[0].equalsIgnoreCase( "reload" ) ) {
                        if ( !main.getTeamspeakManager( ).isAdmin( clientInfo.getDatabaseId( ) ) ) {
                            main.getCommandManager( ).sendMessage( clientInfo, "You don't have permission to perform this command." );
                            return;
                        }
                        reload( );
                        return;
                    }
                }
                sendHelp( clientInfo );
            }

            private void sendHelp( ClientInfo clientInfo ) {
                main.getCommandManager( ).sendMessage( clientInfo, "Here is a help list for the command \'!support\'." );
                main.getCommandManager( ).sendMessage( clientInfo, "- !support accept <id> | Accept a support question" );
                main.getCommandManager( ).sendMessage( clientInfo, "- !support deny <id> | Deny a support question" );
                main.getCommandManager( ).sendMessage( clientInfo, "- !support open | Open the support channel" );
                main.getCommandManager( ).sendMessage( clientInfo, "- !support close | Close the support channel" );
                main.getCommandManager( ).sendMessage( clientInfo, "- !support reload | Reload the \'SupportManager\'." );
            }
        } );
    }

    private void accept( ClientInfo clientInfo, int id ) {

    }

    private void deny( ClientInfo clientInfo, int id ) {

    }

    private void open( ClientInfo clientInfo ) {
        try {
            ChannelInfo channelInfo = main.getTeamspeakManager( ).getTs3Api( ).getChannelInfo( 589 ).get( );

            if ( channelInfo.getName( ).equals( "» Support Queue [Open]" ) ) {
                main.getCommandManager( ).sendMessage( clientInfo, "Der Support ist bereits offen." );
                return;
            }

            HashMap< ChannelProperty, String > properties = Maps.newHashMap( );
            properties.put( ChannelProperty.CHANNEL_NAME, "» Support Queue [Open]" );
            properties.put( ChannelProperty.CHANNEL_MAXCLIENTS, "500" );
            properties.put( ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "1" );
            main.getTeamspeakManager( ).getTs3Api( ).editChannel( channelInfo.getId( ), properties );
            System.out.println( "[TS-Bot] Support is now open" );
            main.getCommandManager( ).sendMessage( clientInfo, "Der Support wurde geöffnet." );
        } catch ( InterruptedException ex ) {
        }
    }

    private void close( ClientInfo clientInfo ) {
        try {
            ChannelInfo channelInfo = main.getTeamspeakManager( ).getTs3Api( ).getChannelInfo( 589 ).get( );

            if ( channelInfo.getName( ).equals( "» Support Queue [Closed]" ) ) {
                main.getCommandManager( ).sendMessage( clientInfo, "Der Support ist bereits geschlossen." );
                return;
            }

            HashMap< ChannelProperty, String > properties = Maps.newHashMap( );
            properties.put( ChannelProperty.CHANNEL_NAME, "» Support Queue [Closed]" );
            properties.put( ChannelProperty.CHANNEL_MAXCLIENTS, "0" );
            properties.put( ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "0" );
            main.getTeamspeakManager( ).getTs3Api( ).editChannel( channelInfo.getId( ), properties );
            System.out.println( "[TS-Bot] Support is now closed" );
            main.getCommandManager( ).sendMessage( clientInfo, "Der Support wurde geschlossen." );
        } catch ( InterruptedException ex ) {
        }
    }

    private void reload( ) {

    }
}
