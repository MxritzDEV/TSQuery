package de.m0xitz.tsquery.support;

import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.google.common.collect.Maps;
import de.m0xitz.tsquery.Main;

import java.util.HashMap;
import java.util.Random;

public class SupportChannel {

    private Main main;
    private ClientInfo user;
    private Random random;
    private int id;

    public SupportChannel( Main main, ClientInfo user ) {
        this.main = main;
        this.user = user;

        init( );
    }

    private void init( ) {
        random = new Random( );
        id = pickRandom( 1000, 1999 );
    }

    public void createChannel( ) {
        try {
            HashMap< ChannelProperty, String > properties = Maps.newHashMap( );
            properties.put( ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "0" );
            properties.put( ChannelProperty.CHANNEL_MAXCLIENTS, "1" );
            properties.put( ChannelProperty.CPID, "589" );

            main.getTeamspeakManager( ).getTs3Api( ).createChannel( "┗ Support Channel [" + id + "]", properties );

            Channel channel = main.getTeamspeakManager( ).getTs3Api( ).getChannelByNameExact( "┗ Support Channel [" + id + "]", false ).get( );
            main.getTeamspeakManager( ).getTs3Api( ).moveClient( user.getId( ), channel.getId( ) );
            main.getTeamspeakManager( ).getTs3Api( ).moveClient( main.getTeamspeakManager( ).getTs3Api( ).whoAmI( ).get( ).getId( ), 763 );
        } catch ( InterruptedException ex ) {
        }
    }

    public void writeMessage( ) {
        try {
            int count = 0;
            for ( Client client : main.getTeamspeakManager( ).getTs3Api( ).getClients( ).get( ) ) {
                if ( !main.getTeamspeakManager( ).hasGroup( client.getDatabaseId( ), 265 ) )
                    continue;
                main.getTeamspeakManager( ).getTs3Api( ).sendPrivateMessage( client.getId( ), "Der User [b]" + user.getNickname( ) + "[/b] braucht Support." );
                count++;
            }
            main.getTeamspeakManager( ).getTs3Api( ).sendPrivateMessage( user.getId( ), "Es wurden [b][color=red]" + count + "[/color][/b] Teammitglieder benachrichtigt." );
        } catch ( InterruptedException ex ) {
        }
    }

    private int pickRandom( int min, int max ) {
        return random.nextInt( max - min + 1 ) + min;
    }

    public ClientInfo getUser( ) {
        return user;
    }

    public int getId( ) {
        return id;
    }
}
