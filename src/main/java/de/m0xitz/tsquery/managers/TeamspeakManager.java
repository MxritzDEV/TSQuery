package de.m0xitz.tsquery.managers;

import com.github.theholywaffle.teamspeak3.TS3ApiAsync;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import de.m0xitz.tsquery.Main;
import de.m0xitz.tsquery.command.Command;
import de.m0xitz.tsquery.support.SupportChannel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TeamspeakManager {

    private Main main;
    private String host;
    private int port;
    private TS3Config ts3Config;
    private TS3Query ts3Query;
    private TS3ApiAsync ts3Api;
    private int clientId;
    private final List< Integer > teamRanks = Lists.newArrayList( 266, 272, 273, 275, 278, 274, 276, 281, 277, 286, 279, 271, 280 );
    private final List< Character > chars = Lists.newArrayList( );
    private final HashMap< ClientInfo, Integer > oldChannels = Maps.newHashMap( );

    public TeamspeakManager( Main main, String host, int port ) {
        this.main = main;
        this.host = host;
        this.port = port;

        init( );
    }

    private void init( ) {
        try {
            System.out.println( "[TS-Bot] Bot is booting" );

            ts3Config = new TS3Config( );
            ts3Config.setHost( host );

            ts3Query = new TS3Query( ts3Config );
            ts3Query.connect( );
            System.out.println( "[TS-Bot] Bot is connected to " + host + ":" + port );

            ts3Api = ts3Query.getAsyncApi( );
            ts3Api.login( "USERNAME", "PASSWORD" );
            ts3Api.selectVirtualServerByPort( port );
            ts3Api.setNickname( "NICKNAME" );

            clientId = ts3Api.whoAmI( ).get( ).getId( );

            ts3Api.moveClient( clientId, 763 );

            registerListener( );
            registerCommands( );
            System.out.println( "[TS-Bot] Bot is successful started" );
        } catch ( InterruptedException ex ) {
        }
    }

    private void registerListener( ) {

        ts3Api.registerAllEvents( );
        ts3Api.addTS3Listeners( new TS3EventAdapter( ) {
            @Override
            public void onClientJoin( ClientJoinEvent event ) {
                try {
                    if ( event.getClientId( ) == clientId )
                        return;
                    String name = event.getInvokerName( );
                    ts3Api.sendPrivateMessage( event.getClientId( ), "\nWillkommen [b]" + name + "[/b] auf dem [b][color=#555555]Ventage.EU[/color] - Teamspeak Server[/b].\nHilfreicher Befehl: [b]!help[/b]" );
                } catch ( Exception e ) {
                    e.printStackTrace( );
                }

            }

            @Override
            public void onClientMoved( ClientMovedEvent event ) {
                try {
                    if ( event.getClientId( ) == clientId )
                        return;
                    ClientInfo clientInfo = ts3Api.getClientInfo( event.getClientId( ) ).get( );
                    ChannelInfo channelInfo = ts3Api.getChannelInfo( event.getTargetChannelId( ) ).get( );
                    if ( channelInfo == null )
                        return;
                    if ( channelInfo.getId( ) == 577 || channelInfo.getId( ) == 578 || channelInfo.getId( ) == 579 || channelInfo.getId( ) == 763 || channelInfo.getId( ) == 580 || channelInfo.getId( ) == 572 || channelInfo.getId( ) == 574 || channelInfo.getId( ) == 576 || channelInfo.getId( ) == 571 ) {
                        if ( isInServerTeam( clientInfo.getDatabaseId( ) ) )
                            return;
                        ts3Api.moveClient( clientInfo.getId( ), 592 );
                        return;
                    }
                    if ( channelInfo.getId( ) != 589 )
                        return;

                    if ( isInServerTeam( clientInfo.getDatabaseId( ) ) )
                        return;

                    SupportChannel supportChannel = new SupportChannel( main, clientInfo );
                    supportChannel.createChannel( );
                    supportChannel.writeMessage( );
                } catch ( InterruptedException ex ) {
                }
            }

            @Override
            public void onTextMessage( TextMessageEvent event ) {
                try {
                    if ( event.getInvokerId( ) == clientId )
                        return;
                    if ( !event.getMessage( ).startsWith( "!" ) )
                        return;
                    ClientInfo clientInfo = ts3Api.getClientInfo( event.getInvokerId( ) ).get( );
                    String name = event.getMessage( ).split( " " )[0];
                    ArrayList< String > list = Lists.newArrayList( event.getMessage( ).split( " " ) );
                    list.remove( name );
                    main.getCommandManager( ).executeCommand( name.replaceFirst( "!", "" ), clientInfo, list.toArray( new String[0] ) );
                } catch ( Exception ex ) {
                    ex.printStackTrace( );
                }
            }
        } );
        System.out.println( "[TS-Bot] Registerd listeners" );

    }

    private void registerCommands( ) {
        main.getCommandManager( ).registerCommand( new Command( "help", "See the list of all Commands." ) {
            @Override
            public void execute( ClientInfo clientInfo, String... strings ) {
                main.getCommandManager( ).sendMessage( clientInfo, "Here is a list of all Commands from the Server." );
                for ( String key : main.getCommandManager( ).getCommands( ).keySet( ) ) {
                    Command command = main.getCommandManager( ).getCommands( ).get( key );
                    main.getCommandManager( ).sendMessage( clientInfo, "- !" + command.getName( ) + " | " + command.getDescription( ) );
                }
            }
        } );
        main.getCommandManager( ).registerCommand( new Command( "nopoke", "-" ) {
            @Override
            public void execute( ClientInfo clientInfo, String... strings ) {
                if ( hasGroup( clientInfo.getDatabaseId( ), 257 ) ) {
                    main.getCommandManager( ).sendMessage( clientInfo, "Du musst dich vorher Verifizieren um diesen Command zu nutzen." );
                    return;
                }
                if ( hasGroup( clientInfo.getDatabaseId( ), 267 ) ) {
                    ts3Api.removeClientFromServerGroup( 267, clientInfo.getDatabaseId( ) );
                    main.getCommandManager( ).sendMessage( clientInfo, "Du wurdest von der Gruppe \'● NoPoke\' entfernt." );
                    return;
                }
                ts3Api.addClientToServerGroup( 267, clientInfo.getDatabaseId( ) );
                main.getCommandManager( ).sendMessage( clientInfo, "Du wurdest der Gruppe \'● NoPoke\' hinzugefügt." );
            }
        } );
        main.getCommandManager( ).registerCommand( new Command( "nomsg", "-" ) {
            @Override
            public void execute( ClientInfo clientInfo, String... strings ) {
                if ( hasGroup( clientInfo.getDatabaseId( ), 257 ) ) {
                    main.getCommandManager( ).sendMessage( clientInfo, "Du musst dich vorher Verifizieren um diesen Command zu nutzen." );
                    return;
                }
                if ( hasGroup( clientInfo.getDatabaseId( ), 268 ) ) {
                    ts3Api.removeClientFromServerGroup( 268, clientInfo.getDatabaseId( ) );
                    main.getCommandManager( ).sendMessage( clientInfo, "Du wurdest von der Gruppe \'● NoMsg\' entfernt." );
                    return;
                }
                ts3Api.addClientToServerGroup( 268, clientInfo.getDatabaseId( ) );
                main.getCommandManager( ).sendMessage( clientInfo, "Du wurdest der Gruppe \'● NoMsg\' hinzugefügt." );
            }
        } );
        main.getCommandManager( ).registerCommand( new Command( "list", "See a list with all users." ) {
            @Override
            public void execute( ClientInfo clientInfo, String... strings ) {
                if ( !isInServerTeam( clientInfo.getDatabaseId( ) ) ) {
                    main.getCommandManager( ).sendMessage( clientInfo, "You don't have permission to perform this command." );
                    return;
                }
                try {
                    boolean isAdmin = isAdmin( clientInfo.getDatabaseId( ) );
                    main.getCommandManager( ).sendMessage( clientInfo, "Online: " + ts3Api.getClients( ).get( ).size( ) + " / 100" );
                    for ( Client client : ts3Api.getClients( ).get( ) ) {
                        main.getCommandManager( ).sendMessage( clientInfo, "- " + client.getNickname( ) + ( isAdmin ? " [" + client.getIp( ) + "]" : "" ) + " | " + client.getId( ) );
                    }
                } catch ( InterruptedException e ) {
                    e.printStackTrace( );
                }
            }
        } );
        main.getCommandManager( ).registerCommand( new Command( "move", "Move users to your channel." ) {
            @Override
            public void execute( ClientInfo clientInfo, String... strings ) {
                if ( !isAdmin( clientInfo.getDatabaseId( ) ) ) {
                    main.getCommandManager( ).sendMessage( clientInfo, "You don't have permission to perform this command." );
                    return;
                }
                try {
                    if ( strings.length == 1 ) {
                        if ( strings[0].equalsIgnoreCase( "all" ) ) {
                            for ( Client client : ts3Api.getClients( ).get( ) ) {
                                if ( client.getId( ) == clientId )
                                    continue;
                                if ( hasGroup( client.getDatabaseId( ), 270 ) )
                                    continue;
                                if ( client.getChannelId( ) == clientInfo.getChannelId( ) )
                                    continue;
                                oldChannels.put( ts3Api.getClientInfo( client.getId( ) ).get( ), client.getChannelId( ) );
                                ts3Api.moveClient( client.getId( ), clientInfo.getChannelId( ) );
                            }
                            return;
                        }
                        if ( strings[0].equalsIgnoreCase( "team" ) ) {
                            for ( Client client : ts3Api.getClients( ).get( ) ) {
                                if ( client.getId( ) == clientId )
                                    continue;
                                if ( !isInServerTeam( client.getDatabaseId( ) ) )
                                    continue;
                                if ( client.getChannelId( ) == clientInfo.getChannelId( ) )
                                    continue;
                                oldChannels.put( ts3Api.getClientInfo( client.getId( ) ).get( ), client.getChannelId( ) );
                                ts3Api.moveClient( client.getId( ), clientInfo.getChannelId( ) );
                            }
                            return;
                        }
                        if ( strings[0].equalsIgnoreCase( "reset" ) ) {
                            for ( ClientInfo info : oldChannels.keySet( ) ) {
                                ts3Api.moveClient( info.getId( ), oldChannels.get( info ) );
                            }
                            return;
                        }
                        try {
                            int groupId = Integer.parseInt( strings[0] );
                            for ( Client client : ts3Api.getClients( ).get( ) ) {
                                if ( client.getId( ) == clientId )
                                    continue;
                                if ( !hasGroup( client.getDatabaseId( ), groupId ) )
                                    continue;
                                if ( client.getChannelId( ) == clientInfo.getChannelId( ) )
                                    continue;
                                oldChannels.put( ts3Api.getClientInfo( client.getId( ) ).get( ), client.getChannelId( ) );
                                ts3Api.moveClient( client.getId( ), clientInfo.getChannelId( ) );
                            }
                            return;
                        } catch ( NumberFormatException ex ) {

                        }
                    }
                    main.getCommandManager( ).sendMessage( clientInfo, "Here is a help list for the command \'!move\'." );
                    main.getCommandManager( ).sendMessage( clientInfo, "- !move all | Move all members in your channel" );
                    main.getCommandManager( ).sendMessage( clientInfo, "- !move team | Move all teammembers in your channel" );
                    main.getCommandManager( ).sendMessage( clientInfo, "- !move reset | Move all members in her old channel" );
                    main.getCommandManager( ).sendMessage( clientInfo, "- !move <groupId> | Move all members of a group in your channel" );
                } catch ( Exception ex ) {
                    ex.printStackTrace( );
                }
            }
        } );
        main.getCommandManager( ).registerCommand( new Command( "stop", "Stop the Teamspeak-Bot." ) {
            @Override
            public void execute( ClientInfo clientInfo, String... strings ) {
                if ( !isAdmin( clientInfo.getDatabaseId( ) ) ) {
                    main.getCommandManager( ).sendMessage( clientInfo, "You don't have permission to perform this command." );
                    return;
                }
                shutdown( );
            }
        } );
    }

    public TS3ApiAsync getTs3Api( ) {
        return ts3Api;
    }

    public boolean isInServerTeam( int databaseId ) {
        try {
            for ( ServerGroup serverGroup : ts3Api.getServerGroupsByClientId( databaseId ).get( ) ) {
                if ( teamRanks.contains( serverGroup.getId( ) ) )
                    return true;
            }
            return false;
        } catch ( InterruptedException ex ) {
            return false;
        }
    }

    public boolean isAdmin( int databaseId ) {
        try {
            for ( ServerGroup serverGroup : ts3Api.getServerGroupsByClientId( databaseId ).get( ) ) {
                if ( Lists.newArrayList( 266, 272 ).contains( serverGroup.getId( ) ) )
                    return true;
            }
            return false;
        } catch ( InterruptedException ex ) {
            return false;
        }
    }

    public boolean hasGroup( int databaseId, int groupId ) {
        try {
            for ( ServerGroup serverGroup : ts3Api.getServerGroupsByClientId( databaseId ).get( ) ) {
                if ( serverGroup.getId( ) == groupId ) {
                    return true;
                }
            }
            return false;
        } catch ( InterruptedException ex ) {
            return false;
        }
    }

    public void shutdown( ) {
        ts3Query.exit( );
        System.out.println( "[TS-Bot] Bot has leaving the server" );
        System.out.println( "[TS-Bot] Bot is stopping" );
        System.exit( 0 );
    }
}
