package de.m0xitz.tsquery.managers;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.google.common.collect.Maps;
import de.m0xitz.tsquery.Main;
import de.m0xitz.tsquery.command.Command;

import java.util.HashMap;

public class CommandManager {

    private Main main;
    private HashMap< String, Command > commands;

    public CommandManager( Main main ) {
        this.main = main;

        init( );
    }

    private void init( ) {
        commands = Maps.newHashMap( );
    }

    public void registerCommand( Command command ) {
        if ( commands.containsKey( command.getName( ) ) )
            return;
        commands.put( command.getName( ), command );
        System.out.println( "[TS-Bot] Command " + command.getName( ) + " registerd" );
    }

    public void executeCommand( String name, ClientInfo clientInfo, String... strings ) {
        System.out.println( "[TS-Bot] " + clientInfo.getNickname( ) + " [" + clientInfo.getId( ) + "] issued server command: !" + name );
        Command command = commands.get( name );
        if ( command == null )
            return;
        command.execute( clientInfo, strings );
    }

    public void sendMessage( ClientInfo clientInfo, String message ) {
        if ( clientInfo == null )
            return;
        try {
            main.getTeamspeakManager( ).getTs3Api( ).sendPrivateMessage( clientInfo.getId( ), message ).get( );
        } catch ( InterruptedException e ) {
        }
    }

    public HashMap< String, Command > getCommands( ) {
        return commands;
    }
}
