package de.m0xitz.tsquery;

import de.m0xitz.tsquery.managers.CommandManager;
import de.m0xitz.tsquery.managers.TeamspeakManager;
import de.m0xitz.tsquery.support.SupportManager;

public class Main {

    private CommandManager commandManager;
    private TeamspeakManager teamspeakManager;
    private SupportManager supportManager;

    private Main( ) {
        commandManager = new CommandManager( this );
        teamspeakManager = new TeamspeakManager( this, "82.211.15.81", 9990 );
        supportManager = new SupportManager( this );
    }

    public static void main( String[] args ) {
        new Main( );
    }

    public CommandManager getCommandManager( ) {
        return commandManager;
    }

    public TeamspeakManager getTeamspeakManager( ) {
        return teamspeakManager;
    }

    public SupportManager getSupportManager( ) {
        return supportManager;
    }
}
